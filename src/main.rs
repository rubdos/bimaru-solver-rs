#![feature(conservative_impl_trait,test)]

extern crate toml;
#[macro_use]
extern crate serde_derive;
extern crate rayon;
#[cfg(test)]
extern crate test;

use std::io::{Read, Write};

use rayon::iter::ParallelIterator;

mod bimaru;
mod permute;

#[derive(Deserialize)]
struct InputFile {
    title: String,
    goal: GoalSpec,
    field: FieldSpec,
}

#[derive(Deserialize)]
struct GoalSpec {
    horizontal: Vec<u8>,
    vertical: Vec<u8>,
    boats: Vec<u8>,
}

#[derive(Deserialize)]
struct FieldSpec {
    up: Option<Vec<(u8, u8)>>,
    down: Option<Vec<(u8, u8)>>,
    left: Option<Vec<(u8, u8)>>,
    right: Option<Vec<(u8, u8)>>,
    single: Option<Vec<(u8, u8)>>,
    centre: Option<Vec<(u8, u8)>>,
    water: Option<Vec<(u8, u8)>>,
}

impl From<InputFile> for bimaru::Field<bimaru::InitialCell> {
    fn from(input: InputFile) -> bimaru::Field<bimaru::InitialCell> {
        use bimaru::*;

        let width: u8 = input.goal.horizontal.len() as u8;
        let height: u8 = input.goal.vertical.len() as u8;
        let size = width * height;

        let mut field = Field::<InitialCell> {
            field: (0..size).map(|_| None).collect(),
            goal: Goal {
                boats: input.goal.boats,
                vertical_goal: input.goal.horizontal,
                horizontal_goal: input.goal.vertical,
            },
            width: width,
            height: height,
        };

        let spec = vec![
            (input.field.up, InitialCell::Top),
            (input.field.down, InitialCell::Bottom),
            (input.field.left, InitialCell::Left),
            (input.field.right, InitialCell::Right),
            (input.field.water, InitialCell::Water),
            (input.field.centre, InitialCell::Centre),
            (input.field.single, InitialCell::Single),
        ];

        for (coordspec, value) in spec {
            if let Some(coords) = coordspec {
                for (x, y) in coords {
                    let x = x - 1;
                    let y = y - 1;
                    field.set(x as i16, y as i16, value.clone());
                }
            }
        }

        field
    }
}

fn main() {
    let filename = std::env::args().nth(1).expect("First argument should be the file!");
    let mut file = std::fs::File::open(filename.clone()).expect("Could not open file");
    let mut contents = String::new();
    file.read_to_string(&mut contents).expect("Could not read file");
    let input: InputFile = toml::from_str(&contents).expect("Could not parse file");

    let field: bimaru::Field<bimaru::InitialCell> = input.into();

    let filename_input = filename.clone() + ".input.tex";
    let mut file = std::fs::File::create(filename_input).expect("Could not open input.tex for writing");
    file.write(field.as_latex().as_bytes());

    println!("{}", field);

    let mut field = field.into_intermediate();
    for _ in 0..4 {
        field.fill_water();
        field.fill_boats();
        field.add_water();
    }
    println!();
    println!("{}", field);
    println!();

    let variations = field.variations().filter(|x| x.is_correct());
    for variation in variations {
        let variation = variation.into_initial().expect("Could not transform into extended notation");
        println!("{}", variation);
        let filename = filename.clone() + ".solution.tex";
        let mut file = std::fs::File::create(filename).expect("Could not open solution for writing");
        file.write(variation.as_latex().as_bytes());
    }
}
