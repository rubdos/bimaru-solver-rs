//use rayon::iter::ParallelIterator;
//use rayon::iter::internal::UnindexedConsumer;

pub struct LexicographicBitPermutation {
    start: i64,
    next: i64,
    n: i64,
    k: i64,
    i: i64,
}

impl LexicographicBitPermutation {
    pub fn new(n: i64, k: i64) -> Self {
        assert!(n >= k);
        let start: i64 = (1i64 << k) - 1;
        LexicographicBitPermutation {
            start: start,
            next: start,
            n: n,
            k: k,
            i: 0,
        }
    }
}

impl Iterator for LexicographicBitPermutation {
    type Item = i64;
    fn next(&mut self) -> Option<i64> {
        // https://stackoverflow.com/questions/1851134/generate-all-binary-strings-of-length-n-with-k-bits-set
        let current = self.next;
        // unsigned int v; // current permutation of bits
        // unsigned int w; // next permutation of bits
        //
        // unsigned int t = v | (v - 1); // t gets v's least significant 0 bits set to 1
        let t = current | (current - 1);
        // // Next set to 1 the most significant bit to change,
        // // set to 0 the least significant ones, and add the necessary 1 bits.
        // w = (t + 1) | (((~t & -~t) - 1) >> (__builtin_ctz(v) + 1));
        self.next = (t + 1) | (((!t & -(!t)) - 1) >> (current.trailing_zeros() + 1));
        if current & (1i64 << self.n) != 0 {
            return None
        }
        assert!(current.count_ones() == self.k as u32);
        self.i += 1;
        Some(current)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;
    #[bench]
    fn test(b: &mut Bencher) {
        b.iter(|| {
            let perm = LexicographicBitPermutation::new(10, 3);
            perm.last()
        })
    }
}

//impl ParallelIterator for LexicographicBitPermutation {
//    type Item = u64;
//    fn drive_unindexed<C: UnindexedConsumer<Self::Item>>(self, consumer: C) -> C::Result {
//        UnindexedConsumer::
//    }
//}
