use std;
use std::fmt::Display;

use rayon::prelude::*;

use super::permute::*;

#[derive(PartialEq)]
enum Direction {
    Horizontal,
    Vertical,
}

#[derive(Clone)]
pub enum InitialCell {
    Water,
    Top,
    Bottom,
    Left,
    Right,
    Centre,
    Single,
}

impl Display for InitialCell {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        use self::InitialCell::*;
        let val = match self {
            &Water => '~',
            &Top => '^',
            &Bottom => 'v',
            &Left => '<',
            &Right => '>',
            &Centre => '#',
            &Single => 'o',
        };
        write!(f, "{}", val)
    }
}

#[derive(PartialEq, Clone)]
pub enum IntermediateCell {
    Water,
    Boat(bool),
}

impl Display for IntermediateCell {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        use self::IntermediateCell::*;
        let val = match self {
            &Water => '~',
            &Boat(_) => '#',
        };
        write!(f, "{}", val)
    }
}

#[derive(Clone)]
pub struct Goal {
    /// Contains the amount of boats of length `(i + 1)` required at index `i`
    pub boats: Vec<u8>,
    /// Contains the amount of boat fields required at column `i` at index `i`
    pub vertical_goal: Vec<u8>,
    /// Contains the amount of boat fields required at row `i` at index `i`
    pub horizontal_goal: Vec<u8>,
}

#[derive(Clone)]
pub struct Field<T> {
    pub field: Vec<Option<T>>,
    pub goal: Goal,
    pub width: u8,
    pub height: u8,
}

impl<T> Field<T> {
    fn is_complete(&self) -> bool {
        self.field.iter().all(|x| x.is_some())
    }

    pub fn set(&mut self, x: i16, y: i16, val: T) {
        if x < 0 { return }
        if y < 0 { return }
        if x >= self.width as i16 { return }
        if y >= self.height as i16 { return }
        self.field[( x + y * self.width as i16 ) as usize] = Some(val);
    }

    pub fn get(&self, x: u8, y: u8) -> &Option<T> {
        &self.field[(x + y * self.width) as usize]
    }
}

impl<T: PartialEq> Field<T> {
    fn count(&self, val: T) -> (Vec<u8>, Vec<u8>) {
        let mut vertical_count = Vec::with_capacity(self.width as usize);
        for x in 0..self.width {
            let mut amount = 0;
            for y in 0..self.height {
                let cell = self.get(x, y);
                if let &Some(ref val2) = cell {
                    if *val2 == val {
                        amount += 1;
                    }
                }
            }
            vertical_count.push(amount);
        }

        let mut horizontal_count = Vec::with_capacity(self.height as usize);
        for y in 0..self.height {
            let mut amount = 0;
            for x in 0..self.width {
                let cell = self.get(x, y);
                if let &Some(ref val2) = cell {
                    if *val2 == val {
                        amount += 1;
                    }
                }
            }
            horizontal_count.push(amount);
        }

        (vertical_count, horizontal_count)
    }
}

impl Field<InitialCell> {
    pub fn into_intermediate(self) -> Field<IntermediateCell> {
        let mut intermediate = Field {
            field: Vec::with_capacity(self.field.len()),
            goal: self.goal,
            width: self.width,
            height: self.height,
        };

        for cell in self.field.iter() {
            if let &Some(ref cell) = cell {
                match cell {
                    &InitialCell::Water => intermediate.field.push(Some(IntermediateCell::Water)),
                    &InitialCell::Centre => intermediate.field.push(Some(IntermediateCell::Boat(true))),
                    _ => intermediate.field.push(Some(IntermediateCell::Boat(false))),
                }
            } else {
                intermediate.field.push(None);
            }
        }

        let mut x: i16 = 0;
        let mut y: i16 = 0;

        for cell in self.field.into_iter() {
            if let Some(cell) = cell {
                use self::IntermediateCell::*;
                match cell {
                    InitialCell::Water => (),
                    InitialCell::Top => {
                        intermediate.set(x, y + 1, Water);
                        intermediate.set(x - 1, y, Water);
                        intermediate.set(x + 1, y, Water);
                        intermediate.set(x, y - 1, Boat(false));

                        intermediate.set(x - 1, y - 2, Water);
                        intermediate.set(x + 1, y - 2, Water);
                    },
                    InitialCell::Bottom => {
                        intermediate.set(x, y - 1, Water);
                        intermediate.set(x - 1, y, Water);
                        intermediate.set(x + 1, y, Water);
                        intermediate.set(x, y + 1, Boat(false));

                        intermediate.set(x - 1, y + 2, Water);
                        intermediate.set(x + 1, y + 2, Water);
                    },
                    InitialCell::Left => {
                        intermediate.set(x - 1, y, Water);
                        intermediate.set(x, y - 1, Water);
                        intermediate.set(x, y + 1, Water);
                        intermediate.set(x + 1, y, Boat(false));

                        intermediate.set(x + 2, y - 1, Water);
                        intermediate.set(x + 2, y + 1, Water);
                    },
                    InitialCell::Right => {
                        intermediate.set(x + 1, y, Water);
                        intermediate.set(x, y - 1, Water);
                        intermediate.set(x, y + 1, Water);
                        intermediate.set(x - 1, y, Boat(false));

                        intermediate.set(x - 2, y - 1, Water);
                        intermediate.set(x - 2, y + 1, Water);
                    },
                    InitialCell::Centre => (),
                    InitialCell::Single => {
                        intermediate.set(x - 1, y, Water);
                        intermediate.set(x + 1, y, Water);
                        intermediate.set(x, y - 1, Water);
                        intermediate.set(x, y + 1, Water);
                    },
                }

                match cell {
                    InitialCell::Water => (),
                    _ => {
                        // All boat pieces have water on the four corners
                        intermediate.set(x - 1, y - 1, Water);
                        intermediate.set(x - 1, y + 1, Water);
                        intermediate.set(x + 1, y - 1, Water);
                        intermediate.set(x + 1, y + 1, Water);
                    }
                }
            }

            x += 1;
            if x == intermediate.width as i16 {
                y += 1;
                x = 0;
            }
        }

        intermediate
    }

    pub fn as_latex(&self) -> String {
        let mut water = vec![];
        let mut segments = vec![];
        let mut ships = vec![];
        for y in 0..self.height {
            for x in 0..self.width {
                match self.get(x, y) {
                    &None => (),
                    &Some(InitialCell::Water) => water.push((x, y)),
                    &Some(ref boat) => segments.push((x, y, boat.clone()))
                }
            }
        }

        for (count, size) in self.goal.boats.iter().zip(1..) {
            for _ in 0..*count {
                ships.push(format!("{}", size));
            }
        }
        ships.reverse();

        let mut source = String::new();
        source.push_str(&format!("\\begin{{battleship}}[rows={}, columns={}, shipcolor=black]\n", self.height, self.width));
        for (x, y, segment) in segments {
            let segment = match segment {
                InitialCell::Water => unreachable!(),
                InitialCell::Top => "ShipT",
                InitialCell::Bottom => "ShipB",
                InitialCell::Centre => "ShipC",
                InitialCell::Single => "Ship",
                InitialCell::Left => "ShipL",
                InitialCell::Right => "ShipR",
            };
            source.push_str(&format!("\\placesegment{{{}}}{{{}}}{{\\{}}}\n", x + 1, y + 1, segment));
        }
        for (x, y) in water {
            source.push_str(&format!("\\placewater{{{}}}{{{}}}\n", x + 1, y + 1));
        }

        source.push_str("\\shipH{");
        source.push_str(&self.goal.vertical_goal.iter().map(u8::to_string).collect::<Vec<_>>().join(","));
        source.push_str("}\n");
        source.push_str("\\shipV{");
        source.push_str(&self.goal.horizontal_goal.iter().map(u8::to_string).collect::<Vec<_>>().join(","));
        source.push_str("}\n");

        source.push_str("\\shipbox{");
        source.push_str(&ships.join(","));
        source.push_str("}\n");
        source.push_str("\\end{battleship}\n");
        source
    }
}

impl Field<IntermediateCell> {
    fn count_boats(&self) -> (Vec<u8>, Vec<u8>) {
        let mut vertical_count = Vec::with_capacity(self.width as usize);
        for x in 0..self.width {
            let mut amount = 0;
            for y in 0..self.height {
                let cell = self.get(x, y);
                if let &Some(ref val2) = cell {
                    if let IntermediateCell::Boat(_) = *val2 {
                        amount += 1;
                    }
                }
            }
            vertical_count.push(amount);
        }

        let mut horizontal_count = Vec::with_capacity(self.height as usize);
        for y in 0..self.height {
            let mut amount = 0;
            for x in 0..self.width {
                let cell = self.get(x, y);
                if let &Some(ref val2) = cell {
                    if let IntermediateCell::Boat(_) = *val2 {
                        amount += 1;
                    }
                }
            }
            horizontal_count.push(amount);
        }

        (vertical_count, horizontal_count)
    }

    fn count_water(&self) -> (Vec<u8>, Vec<u8>) {
        self.count(IntermediateCell::Water)
    }

    pub fn get_checked(&self, x: i16, y: i16) -> Option<IntermediateCell> {
        if x < 0 || x >= self.width as i16 || y < 0 || y >= self.height as i16 {
            return Some(IntermediateCell::Water)
        }
        self.field[(x + y * self.width as i16) as usize].clone()
    }

    /// If the amount of water equals size - the goal, then the rest is boats
    pub fn fill_boats(&mut self) {
        let (vertical_count, horizontal_count) = self.count_water();
        let (vertical_boat_count, horizontal_boat_count) = self.count_boats();

        for y in 0..self.height as usize {
            if horizontal_count[y] == self.width - self.goal.horizontal_goal[y] {
                // Set all None to Boat
                for x in 0..self.width as usize {
                    if self.get(x as u8, y as u8).is_none() {
                        self.set(x as i16, y as i16, IntermediateCell::Boat(false));
                    }
                }
            } else if self.goal.horizontal_goal[y] - horizontal_boat_count[y] == 1 {
                for x in 0..self.width as usize {
                    if let &Some(IntermediateCell::Boat(true)) = self.get(x as u8, y as u8) {
                        use self::IntermediateCell::*;
                        let x = x as i16;
                        let y = y as i16;

                        let left = self.get_checked(x + 1, y);
                        let right = self.get_checked(x - 1, y);
                        match (left, right) {
                            (None, None)
                                | (None, Some(Water))
                                | (Some(Water), None)
                                | (Some(Water), Some(Water)) => {
                                self.set(x, y - 1, Boat(false));
                                self.set(x, y + 1, Boat(false));
                            }
                            _ => ()
                        }
                    }
                }
            }
        }

        for x in 0..self.width as usize {
            if vertical_count[x] == self.height - self.goal.vertical_goal[x] {
                // Set all None to Boat
                for y in 0..self.height as usize {
                    if self.get(x as u8, y as u8).is_none() {
                        self.set(x as i16, y as i16, IntermediateCell::Boat(false));
                    }
                }
            } else if self.goal.vertical_goal[x] - vertical_boat_count[x] == 1 {
                for y in 0..self.height as usize {
                    if let &Some(IntermediateCell::Boat(true)) = self.get(x as u8, y as u8) {
                        use self::IntermediateCell::*;
                        let x = x as i16;
                        let y = y as i16;

                        let above = self.get_checked(x, y + 1);
                        let below = self.get_checked(x, y - 1);
                        match (above, below) {
                            (None, None)
                                | (None, Some(Water))
                                | (Some(Water), None)
                                | (Some(Water), Some(Water)) => {
                                self.set(x - 1, y, Boat(false));
                                self.set(x + 1, y, Boat(false));
                            }
                            _ => ()
                        }
                    }
                }
            }
        }


        for y in 0..self.height {
            for x in 0..self.width {
                match self.get(x, y) {
                    &Some(IntermediateCell::Boat(is_known_centre)) => {
                        use self::IntermediateCell::*;
                        let x = x as i16;
                        let y = y as i16;
                        if is_known_centre {
                            if self.get_checked(x, y + 1) == Some(IntermediateCell::Water)
                                    || self.get_checked(x, y - 1) == Some(IntermediateCell::Water) {
                                // There is known water above or under us, so we're horizontal.
                                self.set(x - 1, y, Boat(false));
                                self.set(x + 1, y, Boat(false));
                            }
                            if self.get_checked(x + 1, y) == Some(IntermediateCell::Water)
                                    || self.get_checked(x - 1, y) == Some(IntermediateCell::Water) {
                                // There is known water left or right of us, so we're vertical.
                                self.set(x, y - 1, Boat(false));
                                self.set(x, y + 1, Boat(false));
                            }
                        }
                    }
                    _ => (),
                }
            }
        }
    }

    /// If the amount of boats equals the goal, then the rest is water
    pub fn fill_water(&mut self) {
        let (vertical_count, horizontal_count) = self.count_boats();

        for y in 0..self.height as usize {
            if horizontal_count[y] == self.goal.horizontal_goal[y] {
                // Set all None to Water
                for x in 0..self.width as usize {
                    if self.get(x as u8, y as u8).is_none() {
                        self.set(x as i16, y as i16, IntermediateCell::Water);
                    }
                }
            }
        }

        for x in 0..self.width as usize {
            if vertical_count[x] == self.goal.vertical_goal[x] {
                // Set all None to Water
                for y in 0..self.height as usize {
                    if self.get(x as u8, y as u8).is_none() {
                        self.set(x as i16, y as i16, IntermediateCell::Water);
                    }
                }
            }
        }
    }

    pub fn add_water(&mut self) {
        for y in 0..self.height {
            for x in 0..self.width {
                match self.get(x, y) {
                    &None => (),
                    &Some(IntermediateCell::Water) => (),
                    &Some(IntermediateCell::Boat(_)) => {
                        // All boat pieces have water on the four corners
                        use self::IntermediateCell::*;
                        let x = x as i16;
                        let y = y as i16;
                        self.set(x - 1, y - 1, Water);
                        self.set(x - 1, y + 1, Water);
                        self.set(x + 1, y - 1, Water);
                        self.set(x + 1, y + 1, Water);
                    }
                }
            }
        }
    }

    pub fn is_correct(&self) -> bool {
        let (vertical_count, horizontal_count) = self.count_boats();
        for y in 0..self.height {
            if horizontal_count[y as usize] != self.goal.horizontal_goal[y as usize] {
                return false
            }
        }
        for x in 0..self.width {
            if vertical_count[x as usize] != self.goal.vertical_goal[x as usize] {
                return false
            }
        }

        for y in 0..self.height {
            for x in 0..self.width {
                if *self.get(x, y).as_ref().unwrap() == IntermediateCell::Water {
                    continue;
                }

                let x = x as i16;
                let y = y as i16;
                let neighbours = [(x - 1, y - 1),
                        (x - 1, y + 1),
                        (x + 1, y - 1),
                        (x + 1, y + 1)];
                for &(x, y) in neighbours.iter() {
                    if x < 0 { continue }
                    if y < 0 { continue }
                    if x >= self.width as i16 { continue }
                    if y >= self.height as i16 { continue }

                    if *self.get(x as u8, y as u8).as_ref().unwrap() != IntermediateCell::Water {
                        return false
                    }
                }
            }
        }

        let boats = self.calculate_boats();
        if self.goal.boats.len() != boats.len() {
            return false
        }

        for (boat, goal_boat) in boats.iter().zip(self.goal.boats.iter()) {
            if *boat as u8 != *goal_boat {
                return false
            }
        }

        true
    }

    pub fn boat_direction(&self, x: u8, y: u8) -> Option<Direction> {
        if let &Some(IntermediateCell::Boat(_)) = self.get(x, y) {
            let x = x as i16;
            let y = y as i16;
            if self.get_checked(x + 1, y) == Some(IntermediateCell::Water)
                   && self.get_checked(x - 1, y) == Some(IntermediateCell::Water) {
                Some(Direction::Vertical)
            } else {
                assert!(self.get_checked(x, y + 1) == Some(IntermediateCell::Water));
                assert!(self.get_checked(x, y - 1) == Some(IntermediateCell::Water));
                Some(Direction::Horizontal)
            }
        } else {
            None
        }
    }

    pub fn calculate_boats(&self) -> Vec<usize> {
        let mut boats = vec![0];

        {
            let mut increment = |length| {
                while boats.len() < length {
                    boats.push(0);
                }
                boats[length - 1] += 1;
            };

            for x in 0..self.width {
                let mut current_boat = 0;
                for y in 0..self.height {
                    if self.boat_direction(x, y) == Some(Direction::Vertical) {
                        current_boat += 1;
                    } else if current_boat > 0 {
                        increment(current_boat);
                        current_boat = 0;
                    }
                }
                if current_boat > 0 {
                    increment(current_boat);
                }
            }

            for y in 0..self.height {
                let mut current_boat = 0;
                for x in 0..self.width {
                    if self.boat_direction(x, y) == Some(Direction::Horizontal) {
                        current_boat += 1;
                    } else if current_boat > 0 {
                        increment(current_boat);
                        current_boat = 0;
                    }
                }
                if current_boat > 0 {
                    increment(current_boat);
                }
            }
        }

        while boats.len() < self.goal.boats.len() {
            boats.push(0);
        }
        boats
    }

    pub fn variations<'a>(&self) -> impl Iterator<Item=Field<IntermediateCell>> {
        let mut empty_fields = vec![];
        for y in 0..self.height {
            for x in 0..self.width {
                if self.get(x, y).is_none() {
                    empty_fields.push((x,y));
                }
            }
        }

        let base: Field<_> = self.clone();

        let total_boats_required: usize = self.goal.boats.iter().zip(0..)
            .map(|(&count, index)| (count * (index + 1)) as usize).sum();
        let total_placed_boats: usize = self.field.iter()
            .filter(|x| x.is_some() && x.as_ref().unwrap() != &IntermediateCell::Water).count();
        assert!(total_boats_required >= total_placed_boats);
        let total_boats_left = total_boats_required - total_placed_boats;

        let generator = LexicographicBitPermutation::new(empty_fields.len() as i64, total_boats_left as i64);
        generator
            .map(move |mut i| {
                let mut derived = base.clone();
                for &(x, y) in empty_fields.iter() {
                    let bit = i & 0x1;
                    i >>= 1;
                    if bit == 1 {
                        derived.set(x as i16, y as i16, IntermediateCell::Boat(false));
                    } else {
                        derived.set(x as i16, y as i16, IntermediateCell::Water);
                    }
                }
                derived
            })
    }

    pub fn into_initial(self) -> Result<Field<InitialCell>, ()> {
        let mut field = Vec::with_capacity(self.field.len());

        for y in 0..self.height {
            for x in 0..self.width {
                let cell = match self.get(x, y).as_ref().ok_or(())? {
                    &IntermediateCell::Water => InitialCell::Water,
                    &IntermediateCell::Boat(_) => {
                        use self::IntermediateCell::*;
                        let x = x as i16;
                        let y = y as i16;
                        let left = self.get_checked(x - 1, y).ok_or(())?;
                        let right = self.get_checked(x + 1, y).ok_or(())?;
                        let up = self.get_checked(x, y + 1).ok_or(())?;
                        let down = self.get_checked(x, y - 1).ok_or(())?;

                        match (left, up, right, down) {
                            (Water, Water, Water, Water) => InitialCell::Single,
                            (Boat(_), Water, Water, Water) => InitialCell::Right,
                            (Water, Boat(_), Water, Water) => InitialCell::Bottom,
                            (Water, Water, Boat(_), Water) => InitialCell::Left,
                            (Water, Water, Water, Boat(_)) => InitialCell::Top,
                            //
                            (Water, Boat(_), Water, Boat(_)) => InitialCell::Centre,
                            (Boat(_), Water, Boat(_), Water) => InitialCell::Centre,
                            _ => return Err(()),
                        }
                    }
                };

                field.push(Some(cell));
            }
        }

        Ok(Field {
            field: field,
            goal: self.goal,
            width: self.width,
            height: self.height,
        })
    }
}

impl<T: Display> Display for Field<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error> {
        for y in 0..self.height {
            let y = self.height - y - 1;
            for x in 0..self.width {
                match self.get(x, y) {
                    &Some(ref cell) => write!(f, "{} ", cell)?,
                    &None => write!(f, "  ")?,
                };
            }

            writeln!(f, "{}", self.goal.horizontal_goal[y as usize]);
        }

        for x in 0..self.width {
            write!(f, "{} ", self.goal.vertical_goal[x as usize]);
        }
        Ok(())
    }
}
